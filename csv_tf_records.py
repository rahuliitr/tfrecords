"""
    Creates TF Records for train, test and validation.
"""
# pylint: disable=E1101

import sys
import os
import shutil
import tensorflow as tf
import cv2
import skimage
import numpy as np
import pandas as pd
tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.INFO)


class GetAddressLabels:
  """
      Reads all jpg file from directory creates lables and address of images
      and split it into train, valid and test set.
  """

  def __init__(self, paths, modes):
    self.path = "../" + paths + "/"
    self.address_labels = None
    self.mode = modes

  # Utils
  def csv_data(self):
    """
        Reads csv file to get label and image path
    """
    # Read csv file
    data_frame = pd.read_csv(self.path + self.mode + ".csv")

    # column with name label as labels of image
    labels = list(data_frame['label'])

    # get number of classes
    unique_labels = list(set(labels))
    num_class = len(unique_labels)

    # make dictionary of unique labels with numbers
    label_dict = dict(zip(unique_labels, list(range(len(unique_labels)))))
    tokens = [label_dict[label] for label in labels]

    # one hot encoding of labels
    labels = tf.keras.utils.to_categorical(tokens, num_class)
    return labels, num_class

  # utils
  def address_and_labels(self):
    """
        Appends all path and one hot encoded labels
    """
    address_all = []
    label_all = []
    data_frame = pd.read_csv(self.path + self.mode + ".csv")
    paths = list(data_frame['path'])
    labels, _ = self.csv_data()
    for i, _ in enumerate(paths):
      path_i = paths[i]
      label_i = labels[i]
      address_all.append(path_i)
      label_all.append(label_i)
    self.address_labels = list(zip(address_all, label_all))

    return self.address_labels

  def path_and_label(self):
    """
        Train valid split adsress
    """
    addr_lab = self.address_and_labels()
    image_path, image_labels = zip(*addr_lab)
    return image_path, image_labels

  # Utils
  def mode_directory(self):
    """
    Check if there exist a directory for train/valid tfrecords
    delete that and creates new
    """
    if os.path.exists('../tfrecords/' + self.mode + '_tfrecords'):
      shutil.rmtree('../tfrecords/' + self.mode + '_tfrecords')
    os.mkdir('../tfrecords/' + self.mode + '_tfrecords')
    return print(self.mode, "_tfrecords directory created")


class CreateTFRecords(GetAddressLabels):
  """
      Load images, create TFRecords for train, valid and test tfrecords.
  """

  def __init__(self, images_per_record, path, mode):
    GetAddressLabels.__init__(self, paths=path, modes=mode)
    self.image_path, self.image_label = self.path_and_label()
    self.images_per_record = images_per_record

  def tfrecords_directory(self):
    """
        Check if there exist a tfrecords_directory
        delete that and create new directory
    """
    if self.mode == 'train':
      if os.path.exists('../tfrecords'):
        shutil.rmtree('../tfrecords')
      os.mkdir('../tfrecords')
      print("tf_records directory created")
    else:
      pass

  @staticmethod
  def load_image(filename):
    """
        Read images using skimage, reshape, and returns the image.
    """
    image_data = skimage.io.imread(filename)
    if image_data is None:
      return filename
    image_data = cv2.resize(image_data,
                            (244, 244),
                            interpolation=cv2.INTER_CUBIC)
    image_data = cv2.cvtColor(image_data, cv2.COLOR_BGR2RGB)
    image_data = image_data.astype(np.float32)
    return image_data

  # utils
  @staticmethod
  def check_image_size(add):
    """
    Check image size before reading
    if size = 0 returns address of image.
    """
    data_size = os.path.getsize(add)
    if data_size == 0:
      print("Size of image is zero")
    else:
      pass

  @staticmethod
  def _int64_feature(value):
    """
    Create feature of type Int64List
    """
    return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))

  @staticmethod
  def _bytes_feature(value):
    """
        Creates feature of type BytesList
    """
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))

  def create_tfrecords(self):
    """
        Creates Train TFRecords.
    """
    self.mode_directory()
    add = self.image_path
    labels = self.image_label

    for i, _ in enumerate(add):
      label = labels[i]
      self.check_image_size(add[i])
      image = self.load_image(add[i])

      if not i % self.images_per_record:
        print(self.mode, " data {}/{}".format(i, len(add)))
        tfrecord_filename = os.path.join(
            '../tfrecords/' +
            self.mode +
            '_tfrecords/' +
            self.mode +
            '_tfrecords_' +
            str(i) +
            '_.tfrecord')
        writer = tf.io.TFRecordWriter(tfrecord_filename)
      feature = {
          self.mode + '/label': self._bytes_feature(
              tf.compat.as_bytes(
                  label.tostring())),
          self.mode + '/image': self._bytes_feature(
              tf.compat.as_bytes(
                  image.tostring()))}

      example = tf.train.Example(
          features=tf.train.Features(
              feature=feature))
      writer.write(example.SerializeToString())
    writer.close()
    sys.stdout.flush()
