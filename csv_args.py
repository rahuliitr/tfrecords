"""
    Takes argument as input from terminal
"""
import argparse


def get_arguments():
  """
      Arguments input from terminal
  """
  parser = argparse.ArgumentParser()
  parser.add_argument(
      '--images-per-record',
      type=int,
      default=100,
      help='number of images per record')

  parser.add_argument(
      '--directory-path',
      type=str,
      required=True,
      help='path of directory of data')

  parser.add_argument(
      '--mode',
      type=str,
      required=True,
      help='train, test, valid')

  return parser.parse_args()
