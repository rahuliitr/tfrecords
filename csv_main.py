"""
   Main file
"""

from csv_tf_records import CreateTFRecords
from args import get_arguments


def main():
  """
     Main func
  """
  args = get_arguments()
  tfr = CreateTFRecords(images_per_record=args.images_per_record,
                        path=args.directory_path,
                        mode=args.mode)
  tfr.tfrecords_directory()
  tfr.create_tfrecords()
 #  tfr.create_tfrecords('valid')


if __name__ == "__main__":
  main()
